## Start TES-VM

1. We want to use another VM with TES to execute Snakemake tasks. Therefore, we create a new virtual machine:
  - (Details) Set **Create new volume** to **no**
  - (Source) Bootquelle auswählen -> Abbild -> theia
  - (Variant) select **de.NBI tiny** (Größer????)
  - (Network) select **onlineTraining-network**
  - (Key pair) choose **YOURKEY**
  - Create instance
  - Assign floating IP (Which one??)

2. and add new instance to ssh config; e.g. for windows:

```
host training_tes
  HostName 172.16.XXX.XXX
  IdentityFile PATH_TO_KEY
  User ubuntu
  ProxyCommand C:\Windows\System32\OpenSSH\ssh.exe denbi-jumphost-01.bihealth.org -W %h:%p
  LocalForward 8000 localhost:8000
  LocalForward 9090 localhost:9090
  LocalForward 8182 localhost:8181
```

## Docker

1. install docker on tes node

```bash
ssh training_tes
sudo apt-get update
sudo apt-get install -y docker.io
sudo usermod -aG docker ${USER}
```

2. For outgoing connection from docker container, we need to set mtu at /etc/docker/daemon.json and restart docker:

```
sudo echo '{"mtu": 1450}' | sudo tee -a /etc/docker/daemon.json
sudo service docker restart
```

3. Now, exit current shell and re-login into the instance and we can use docker e.g. to list images

```bash
docker images
```

## Funnel

1. To use Funnel, we install conda and a new environment:

```bash
cd $HOME
git clone https://gitlab.com/twardzso/3rd_denbi_user_meeting__snakemake_cloud.git
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
/bin/bash Miniconda3-latest-Linux-x86_64.sh
# type: (just ENTER), yes, (just ENTER), yes
source ~/.bashrc
cd 3rd_denbi_user_meeting__snakemake_cloud/4_tes
conda env create --file environment.yaml
conda activate funnel
```

2. Download funnel and start the server 

```bash
cd $HOME
wget https://github.com/ohsu-comp-bio/funnel/releases/download/0.10.0/funnel-linux-amd64-0.10.0.tar.gz
tar -xvzf funnel-linux-amd64-0.10.0.tar.gz
mkdir scratch
./funnel server --Worker.WorkDir ${HOME}/scratch run
```

5. In our browser we can go to the url localhost:8000 to view funnel dashboard

6. On training_snakemake node, we can test connection to the tes server via curl (please set INTERNAL_IP with your TES-VM IP)

```bash
INTERNAL_IP="10.0.0.13"
curl $INTERNAL_IP:8000/v1/tasks # show all tasks
curl $INTERNAL_IP:8000/v1/tasks/service-info # get server info
```


## install and use snakemake:integrate_tes_v2

1. To use TES execution in Snakemake, we need to install a specific branch of Snakemake  **integrate_tes_v2**, because the feature is not yet implemented in Master branch.

```bash
conda deactivate
cd $HOME
git clone https://github.com/snakemake/snakemake.git
cd snakemake
git checkout integrate_tes_v2
conda env create -f test-environment.yml -n snakemake-tes
conda activate snakemake-tes
pip install -e .
```


2. Run snakemake with TES (do not forget to insert SWIFT keys in Snakefile and also adapt result prefix!)

```bash
export CONDA_PKGS_DIRS=/tmp/conda
export CONDA_ENVS_PATH=/tmp/conda

snakemake --tes http://10.0.0.13:8000 \
  --use-conda \
  --envvars CONDA_PKGS_DIRS CONDA_ENVS_PATH \
  --conda-prefix "/tmp/conda" \
  all
```

