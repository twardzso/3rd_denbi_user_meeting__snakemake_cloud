# Initialization

## 0 Prerequisites

- You need to have an elixir account to become a member of our training project at the de.NBI cloud in Berlin
- Your public ssh must be added at the de.NBI portal: https://cloud.denbi.de/portal/ (or at the elixir portal) to able to connect to the jumphost

## 1 Deploy a vm in training project

1. Login  with elixir account at de.NBI cloud Berlin horizon dashboard (OpenStack graphical interface): https://denbi-cloud.bihealth.org/
2. First time users need to add their public ssh key to the horizon dashboard, to be able to inject your public key when you create a new vm:
_Project --> Compute --> Key Pairs --> Import Public Key_

3. Create a Theia virtual machine: _Project --> Compute --> Instances --> Launch Instance_

    - Details: set an **Instance Name with your zoomname** in it
    - Source: set **Create new volume** to **no** and choose "theia" image
    - Flavor:  **de.NBI default**
    - Networks: select **onlineTraining-network**
    - Key pair: choose **YOURKEY**
    - Launch instance

4. Assign a floating ip to get ssh access: _Project --> Compute --> Instances --> choose your instance --> assign floating vm
 
    - Assign floating IP from range 172.16.102.x or172.16.103.x 

    Please make sure you did not accidentally take a floating ip address from another ip range. They are reserved for another use case and will not work with you vm.

## 2 Connect to instance via ssh

Connection to OpenStack vm at de.NBI cloud site Berlin is only possible via jumphost. Therefore you have two options:

**A)** Manually jump from your client to jumphost and from there further to your vm with [ssh-agent-forwarding](https://www.ssh.com/ssh/agent)

1. Start ssh-agent:

    ```bash
    eval `ssh-agent` // eval $(ssh-agent)
    ```

2. Add ssh private key:

    ```bash
    ssh-add .ssh/id_rsa
   ```

    show identities:

    ```bash
    ssh-add -l
   ```

3. Connect at first from your client to jumphost: 

    ```bash
    ssh -A yourElixirUserName@denbi-jumphost-01.bihealth.org
    ```

    And from the jumphost you can connect further to the floating ip of your vm

    ```bash
    ssh ubuntu@yourFloatingIpOfVM
    ```


**B)** Setup ssh-config under .ssh/config

  - Windows 10:

```bash
Host denbi-jumphost-01.bihealth.org
    HostName denbi-jumphost-01.bihealth.org
    User yourElixirUserName
    IdentityFile PATH_TO_KEY

Host training_snakemake
  HostName 172.16.XXX.XXX
  IdentityFile PATH_TO_KEY
  User ubuntu
  ProxyCommand C:\Windows\System32\OpenSSH\ssh.exe denbi-jumphost-01.bihealth.org -W %h:%p
  LocalForward 8181 localhost:8181
```

  - Linux:

```bash
Host denbi-jumphost-01.bihealth.org
    HostName denbi-jumphost-01.bihealth.org
    User yourElixirUserName
    IdentityFile PATH_TO_KEY

Host training_snakemake
  HostName 172.16.XXX.XXX
  IdentityFile PATH_TO_KEY
  User ubuntu
  ProxyJump denbi-jumphost-01.bihealth.org
  LocalForward 8181 localhost:8181 # other option is to use ssh -L 8181:localhost:8181 ubuntu@training_snakemake
```

## 3. Setup project 

Connect to instance via ssh and to Theia via browser (http://localhost:8181/#/home/ubuntu)

1. Clone repository

```bash
cd $HOME
git clone https://gitlab.com/twardzso/3rd_denbi_user_meeting__snakemake_cloud.git
```

2. Download and install conda (type: *empty*, **yes**, *empty*, **yes**)

```bash
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
/bin/bash Miniconda3-latest-Linux-x86_64.sh
source ~/.bashrc
```

3. Install conda environment

```bash
cd $HOME/3rd_denbi_user_meeting__snakemake_cloud/1_init/
conda env create --file environment.yaml
conda activate snakemake_training
```
