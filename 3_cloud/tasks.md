# Make workflow cloud ready

In this task will modify the workflow from task 2 for cloud execution.
We need to attach a conda environment to each rule and use remote files via S3 for input and output files.

## Add conda environments

1. First, we copy the Snakefile, data and scripts in task 3 directory.

```bash
cd $HOME/3rd_denbi_user_meeting__snakemake_cloud/3_cloud/
cp ../2_snakemake/Snakefile Snakefile
cp -r ../2_snakemake/scripts scripts/
cp -r ../2_snakemake/data data/
```

2. Then, we attach a conda environment (stored at **env/**) to each rule (except all) in the Snakefile. For example:

```python
rule bwa_map:
    input:
        genome="data/genome.fa",
        reads="data/samples/{sample}.fastq"
    output:
        alignment="mapped_reads/{sample}.bam"
    conda:
        "envs/bwa_samtools_bcftools.yaml"
    shell:
        "bwa mem {input.genome} {input.reads} | samtools view -Sb - > {output.alignment}"
```

3. We can now use Snakemake to initialize the rule specific conda environments. Environments will build and stored at .snakemake/conda/XXXXXX

```bash
snakemake --cores 1 --conda-create-envs-only --use-conda all
```

## Swift storage

1. We use the Python package Boto3 to inspect our SWIFT container at the de.NBI Bielefeld site.
Do not forget to insert keys!

```python
import boto3

access_key = ""
secret_access_key = ""

resource = boto3.client('s3',
  endpoint_url='https://openstack.cebitec.uni-bielefeld.de:8080',
  aws_access_key_id=access_key, aws_secret_access_key=secret_access_key)

s3objects = resource.list_objects_v2(Bucket="test-container")
for s3object in s3objects['Contents']:
  print(f'  {s3object["Key"]}')
```

2. Next, we modify the workflow to use our SWIFT storage.
Therefore, we need to initialize using a S3 storage (SWIFT is compatible to S3) at the beginning of the Snakemake workflow.
To prevent collisions, you also need to add a personal prefix for storage of files; e.g. firstname_lastname.
All your created files will be stored with this prefix. 
Do not forget to insert keys!

```python
from snakemake.remote.S3 import RemoteProvider as S3RemoteProvider

S3 = S3RemoteProvider(
    access_key_id="",
    secret_access_key="",
    endpoint_url="https://openstack.cebitec.uni-bielefeld.de:8080")

data_prefix = "test-container/data/"
results_prefix = "test-container/results/XXXXX_XXXXX/"
```

3. Next, we wrap all files with **S3.remote** and adapt file paths.
For **bwa_map**, we also need index files as input.
So we need to list them, such that that they will be downloaded.

```bash
rule bwa_map:
    input:
        genome=S3.remote(data_prefix +  "genome.fa"),
        genome_amb=S3.remote(data_prefix + "genome.fa.amb"),
        genome_ann=S3.remote(data_prefix + "genome.fa.ann"),
        genome_bwt=S3.remote(data_prefix + "genome.fa.bwt"),
        genome_pac=S3.remote(data_prefix + "genome.fa.pac"),
        genome_sa=S3.remote(data_prefix + "genome.fa.sa"),
        reads=S3.remote(data_prefix + "samples/{sample}.fastq")
    output:
        alignment=S3.remote(results_prefix + "mapped_reads/{sample}.bam")
    conda:
        "envs/environment.yaml"
    shell:
        "bwa mem {input.genome} {input.reads} | samtools view -Sb - > {output.alignment}"

#...

SAMPLES = ["A", "B"]

rule bcftools_call:
    input:
        fa=S3.remote(data_prefix + "genome.fa"),
        fai=S3.remote(data_prefix + "genome.fa.fai"),
        bams=S3.remote(expand(results_prefix + "sorted_reads/{sample}.bam", sample=SAMPLES)),
        bais=S3.remote(expand(results_prefix + "sorted_reads/{sample}.bam.bai", sample=SAMPLES))
    output:
        S3.remote(results_prefix + "calls/all.vcf")
    conda:
        "envs/bwa_samtools_bcftools.yaml"
    shell:
        "samtools mpileup -g -f {input.fa} {input.bams} | "
        "bcftools call -mv - > {output}"

#...

```

4. Now, lets run the workflow:

```bash
snakemake --use-conda --cores 1 all
```

5. After execution, the files are stored in our SWIFT container, marked with your personal prefix.
Lets list all files using the python script (insert keys!):

```python
import boto3

access_key = ""
secret_access_key = ""

resource = boto3.client('s3',
  endpoint_url='https://openstack.cebitec.uni-bielefeld.de:8080',
  aws_access_key_id=access_key, aws_secret_access_key=secret_access_key)

s3objects = resource.list_objects_v2(Bucket="test-container")
for s3object in s3objects['Contents']:
  print(f'  {s3object["Key"]}')
```
