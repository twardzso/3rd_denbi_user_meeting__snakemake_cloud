# Snakemake

## Data

1. First, we download tutorial data from the official Snakemake repository - complete tutorial is very recommended for advanced learning!
  - ignore the environment.yaml file

```bash
conda activate snakemake_training
cd $HOME/3rd_denbi_user_meeting__snakemake_cloud/2_snakemake
wget https://github.com/snakemake/snakemake-tutorial-data/archive/v5.24.1.tar.gz
tar --wildcards -xf v5.24.1.tar.gz --strip 1 "*/data" "*/environment.yaml"
```

##  A simple workflow

1. The first workflow executes bwa mem at some input reads and transforms resulting alignment file into BAM format.

```python
rule bwa_map:
    input:
        genome="data/genome.fa",
        reads="data/samples/A.fastq"
    output:
        alignment="mapped_reads/A.bam"
    shell:
        "bwa mem {input.genome} {input.reads} | samtools view -Sb - > {output.alignment}"
```

2. We create a workflow file (name: Snakefile) and copy the code into it. Run the workflow with:

```bash
snakemake -n mapped_reads/A.bam
snakemake --cores 1 mapped_reads/A.bam
```

3. Snakemake uses wildcards for generalization; here {sample} is used to generalize input reads:

```python
rule bwa_map:
    input:
        genome="data/genome.fa",
        reads="data/samples/{sample}.fastq"
    output:
        alignment="mapped_reads/{sample}.bam"
    shell:
        "bwa mem {input.genome} {input.reads} | samtools view -Sb - > {output.alignment}"
```

4. Run new workflow:

```bash
snakemake -n mapped_reads/A.bam mapped_reads/B.bam
```

5. Snakemake will execute sample B only, because we already executed sample A. In this example, to rerun sample A we could simulate new data for sample A (using touch) or delete alignment A:

```bash
touch data/samples/A.fastq
snakemake -n mapped_reads/A.bam mapped_reads/B.bam
```

## Advanced workflow

1. We add more steps to the workflow; sort alignments using samtools:

```python
rule samtools_sort:
    input:
        bam="mapped_reads/{sample}.bam"
    output:
        bam="sorted_reads/{sample}.bam"
    shell:
        "samtools sort -T sorted_reads/{wildcards.sample} -O bam {input.bam} > {output.bam}"
```

2. and create an alignment index file:

```python
rule samtools_index:
    input:
        bam="sorted_reads/{sample}.bam"
    output:
        bai="sorted_reads/{sample}.bam.bai"
    shell:
        "samtools index {input.bam}"
```

3. Lets also call some small nucleotide variants:

```python
SAMPLES = ["A", "B"]

rule bcftools_call:
    input:
        fa="data/genome.fa",
        bams=expand("sorted_reads/{sample}.bam", sample=SAMPLES),
        bais=expand("sorted_reads/{sample}.bam.bai", sample=SAMPLES)
    output:
        "calls/all.vcf"
    shell:
        "samtools mpileup -g -f {input.fa} {input.bams} | "
        "bcftools call -mv - > {output}"
```

4. And we apply a custom script to visualize the results:

```python
rule plot_quals:
    input:
        "calls/all.vcf"
    output:
        "plots/quals.svg"
    script:
        "scripts/plot-quals.py"
```

5. Therefore, we need to add the python script to "$HOME/3rd_denbi_user_meeting__snakemake_cloud/2_snakemake/scripts/plot-quals.py":

```python
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
from pysam import VariantFile

quals = [record.qual for record in VariantFile(snakemake.input[0])]
plt.hist(quals)

plt.savefig(snakemake.output[0])
```

6. It is good practice to add a initial all rule at the beginning at the workflow, which will be executed by default

```python
rule all:
    input:
        "plots/quals.svg"
```

7. Lets use Snakemake to visualize the execution DAG; files can be downloaded e.g using right-click in theia or via scp.

```
snakemake --dag all | dot -Tsvg > dag.svg
```

8. Run it all!

```
snakemake -n
snakemake --cores 1 all
```
